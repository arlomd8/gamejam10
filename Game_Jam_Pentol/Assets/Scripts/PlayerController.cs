﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float jumpForce;
    public int maxLife = 3;
    public int currentLife;

    public KeyCode jump;
    public KeyCode attack;
    public KeyCode gravity;

    private Rigidbody2D theRB;

    public Transform groundCheckPoint;
    public float groundCheckRadius;
    public LayerMask whatisGround;

    public Transform attackPoint;
    public float attackRadius;
    public LayerMask whatisAttack;

    public bool isGrounded;
    public bool isGravity;

    public Animator anim;

    public enum GravityDirection { Down, Up };
    GravityDirection m_GravityDirection;

    void Start()
    {
        theRB = GetComponent<Rigidbody2D>();
        currentLife = maxLife;
        //anim = GetComponent<Animator>();
    }
    IEnumerator Flip()
    {
        yield return new WaitForSeconds(0.6f);
        this.transform.localScale *= new Vector2(1, -1);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Enemy") || collision.collider.CompareTag("Obstacle"))
        {
            StartCoroutine(Respawn());
        }
    }
    
    IEnumerator Respawn()
    {
        print("Respawn");
        currentLife -= 1;
        yield return new WaitForSeconds(3f);

    }

    private void OnDrawGizmos()
    {
        if (attackPoint == null)
            return;

        Gizmos.DrawWireSphere(attackPoint.position, attackRadius);
        Gizmos.DrawWireSphere(groundCheckPoint.position, groundCheckRadius);
    }

    void Update()
    {
        if(currentLife <= 0)
        {
            GameOver();
        }

        isGrounded = Physics2D.OverlapCircle(groundCheckPoint.position, groundCheckRadius, whatisGround);

        switch (m_GravityDirection)
        {
            case GravityDirection.Down:
                Physics2D.gravity = new Vector2(0, -9.8f);
                if (Input.GetKeyDown(gravity) || isGravity)
                {
                    m_GravityDirection = GravityDirection.Up;
                    StartCoroutine(Flip());
                }
                break;

            case GravityDirection.Up:
                Physics2D.gravity = new Vector2(0, 9.8f);
                if (Input.GetKeyDown(gravity) || !isGravity)
                {
                    m_GravityDirection = GravityDirection.Down;
                    StartCoroutine(Flip());
                }
                break;
        }

        if (Input.GetKeyDown(jump) && isGrounded)
        {
            theRB.velocity = new Vector2(theRB.velocity.x, jumpForce);
        }

        if (Input.GetKeyDown(attack))
        {
            //anim.SetTrigger("Attack");
            Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRadius, whatisAttack);

            foreach(Collider2D enemy in hitEnemies)
            {
                enemy.GetComponent<Enemy>().TakeDamage(100);
            }
        }
        if(theRB.velocity.x < 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        else if(theRB.velocity.x > 0)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }


    }

    void GameOver()
    {
        //GameOver
    }

#if UNITY_ANDROID
    
    public void Jump()
    {
        if (isGrounded)
        {
            theRB.velocity = new Vector2(theRB.velocity.x, jumpForce);
        }

        if (isGravity && isGrounded)
        {
            theRB.velocity = new Vector2(theRB.velocity.x, -jumpForce);
        }
    }

    public void Gravity()
    {
        if (isGravity)
        {
            isGravity = false;
        }
        else
        {
            isGravity=true;
        }
    }

    public void Attack()
    {
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRadius, whatisAttack);

        foreach (Collider2D enemy in hitEnemies)
        {
            enemy.GetComponent<Enemy>().TakeDamage(100);
        }
    }
    


#endif
}